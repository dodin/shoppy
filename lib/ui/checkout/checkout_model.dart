import 'package:flutter/foundation.dart';
import 'package:get_it/get_it.dart';
import 'package:shoppy_template/model/cart.dart';
import 'package:shoppy_template/model/dto/order.dart';
import 'package:shoppy_template/model/repository.dart';

class CheckoutModel extends ChangeNotifier {
  Repository _repo = GetIt.I.get<Repository>();

  String _payment;
  String get payment => _payment;
  set payment(String value) {
    _payment = value;
    notifyListeners();
  }

  String _shipping;
  String get shipping => _shipping;
  set shipping(String value) {
    _shipping = value;
    notifyListeners();
  }

  String address = '';

  Set<String> _shippingMethods = {};
  Set<String> get shippingMethods => _shippingMethods;

  Set<String> _paymentMethods = {};
  Set<String> get paymentMethods => _paymentMethods;


  CheckoutModel() {
    _load();
  }

  void _load() async {
    _shippingMethods = await _repo.loadShippingMethods();
    shipping = _shippingMethods.first;
    _paymentMethods = await _repo.loadPaymentMethods();
    payment = _paymentMethods.first;
    final profile = await _repo.loadProfile();
    address = profile.address;
    notifyListeners();
  }

  Future<bool> saveOrder(String orderNum) async {
    final itemsInCart = await _repo.loadCart();
    final cart = Cart(items: itemsInCart);

    final order = Order(
      paymentMethod: _payment,
      paymentDetails: orderNum,
      paymentStatus: orderNum == null ? 'In process' : 'Payed',
      address: address,
      shippingMethod: _shipping,
      amount: cart.totalPrice,
      cartItems: cart.items
    );

    var result = await _repo.saveOrder(order);
    if (result) {
      _repo.clearCart();
    }
    return result;
  }
}