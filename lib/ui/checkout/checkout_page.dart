import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shoppy_template/common/payments/payments_factory.dart';
import 'package:shoppy_template/localization.dart';
import 'package:shoppy_template/router.dart';
import 'package:shoppy_template/ui/cart/cart_model.dart';

import 'checkout_model.dart';

class CheckoutPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final locale = AppLocalizations.of(context);
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: ChangeNotifierProvider(
        create: (_) => CheckoutModel(),
        child: Container(
          padding: EdgeInsets.all(16),
          child: Padding(
            padding: EdgeInsets.only(top: 48),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(locale.checkoutTitle, style: TextStyle(fontSize: 28)),
                Divider(),
                SizedBox(height: 16),
                Text(locale.checkoutAddress, style: TextStyle(fontSize: 20)),
                SizedBox(height: 16),
                Consumer<CheckoutModel>(
                  builder: (context, model, child) {
                    TextEditingController controller =
                      TextEditingController(text: model.address);
                    return TextFormField(
                      controller: controller,
                      decoration: InputDecoration(labelText: locale.checkoutAddress),
                      validator: (value) {
                        if (value.trim().isEmpty) {
                          return '';
                        }
                        return null;
                      },
                      onEditingComplete: () => model.address = controller.value.text,
                    );
                  },
                ),
                SizedBox(height: 32),
                Text(locale.checkoutPayment, style: TextStyle(fontSize: 20)),
                SizedBox(height: 16),
                Consumer<CheckoutModel>(
                  builder: (context, model, child) => Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      DropdownButton(
                        value: model.payment,
                        items: model.paymentMethods.map((item) {
                          return DropdownMenuItem(
                            value: item,
                            child: Text(item),
                          );
                        }).toList(),
                        onChanged: (String value) => model.payment = value,
                      ),
                      PaymentsFactory.getPayment(model.payment) != null ?
                      PaymentsFactory.getPayment(model.payment).getForm(context) :
                      SizedBox.shrink()
                    ],
                  ),
                ),
                SizedBox(height: 32),
                Text(locale.checkoutShipping, style: TextStyle(fontSize: 20)),
                SizedBox(height: 16),
                Consumer<CheckoutModel>(
                  builder: (context, model, child) => DropdownButton(
                    value: model.shipping,
                    items: model.shippingMethods.map((item) {
                      return DropdownMenuItem(
                        value: item,
                        child: Text(item),
                      );
                    }).toList(),
                    onChanged: (String value) => model.shipping = value,
                  ),
                ),
                Expanded(
                  child: SizedBox(),
                ),
                Consumer<CheckoutModel>(
                  builder: (context, model, child) => MaterialButton(
                    minWidth: double.infinity,
                    color: Colors.red,
                    child: Text(locale.checkoutOrder, style: TextStyle(color: Colors.white)),
                    onPressed: () {
                      final paymentMethod = PaymentsFactory.getPayment(model.payment);
                      if (paymentMethod != null) {
                        paymentMethod.execute(context, (orderNum) async {
                          final result = await model.saveOrder(orderNum);
                          if (result) {
                            CartModel cartModel = context.read<CartModel>();
                            cartModel.clear();
                            Navigator.popUntil(context, ModalRoute.withName(Routes.mainPage));
                          } else {
                            Scaffold.of(context).showSnackBar(SnackBar(
                              content: Text(locale.errorTryAgain),
                            ));
                          }
                        });
                      }
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}