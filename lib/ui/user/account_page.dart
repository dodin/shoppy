import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shoppy_template/localization.dart';
import 'package:shoppy_template/ui/user/anonymous_widget.dart';
import 'package:shoppy_template/ui/user/registered_widget.dart';

import 'account_model.dart';

class AccountPage extends StatelessWidget {
  @override
  Widget build(BuildContext ctx) {
    return ChangeNotifierProvider<AccountModel>(
      create: (_) => AccountModel(),
      child: SafeArea(
        child: Consumer<AccountModel>(
          builder: (context, model, child) {
            return Scaffold(
              appBar: AppBar(
                title: Text(AppLocalizations.of(ctx).accountTitle),
              ),
              body: model.isAnonymous ? AnonymousWidget() : RegisteredWidget(),
            );
          },
        ),
      ),
    );
  }
}
