import 'package:email_validator/email_validator.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';
import 'package:get_it/get_it.dart';
import 'package:shoppy_template/model/authentication.dart';
import 'package:shoppy_template/model/dto/order.dart';
import 'package:shoppy_template/model/repository.dart';

class AccountModel extends ChangeNotifier {
  Authentication _auth = GetIt.I<Authentication>();
  Repository _repo = GetIt.I<Repository>();

  bool get isAnonymous => _auth.user == null || _auth.user.isAnonymous;
  User get user => _auth.user;

  List<Order> orders = List();
  String address = "";

  String _email;
  String _password;

  String error;

  AccountModel() {
    _init();
  }

  ValidationResult validateEmail(String value) {
    value = value.trim();
    if (value.isEmpty) {
      return ValidationResult.EmptyEmail;
    }
    if (!EmailValidator.validate(value)) {
      return ValidationResult.WrongEmail;
    }

    _email = value;
    return ValidationResult.Ok;
  }

  ValidationResult validatePassword(String value) {
    if (value.isEmpty) {
      return ValidationResult.EmptyPassword;
    }

    _password = value;
    return ValidationResult.Ok;
  }

  Future<bool> login() async {
    final result = await _auth.signInWithEmail(_email, _password);

    if (result.hasErrors()) {
      error = result.e.message;
      return false;
    }

    return true;
  }

  Future<bool> register() async {
    final result = await _auth.registerWithEmail(_email, _password);

    if (result.hasErrors()) {
      error = result.e.message;
      return false;
    }

    notifyListeners();
    return true;
  }

  void saveAddress(String address) {
    _repo.updateProfile(address: address);
  }

  _init() async {
    final profile = await _repo.loadProfile();
    address = profile.address;

    orders = await _repo.loadOrderHistory();
    notifyListeners();
  }
}

enum ValidationResult {
  Ok, EmptyEmail, WrongEmail, EmptyPassword
}