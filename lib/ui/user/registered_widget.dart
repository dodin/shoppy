import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shoppy_template/common/Date.dart';
import 'package:shoppy_template/localization.dart';
import 'package:shoppy_template/router.dart';
import 'package:shoppy_template/ui/item/item_page.dart';
import 'package:shoppy_template/ui/user/account_model.dart';

class RegisteredWidget extends StatefulWidget {
  @override
  _RegisteredWidgetState createState() => _RegisteredWidgetState();
}

class _RegisteredWidgetState extends State<RegisteredWidget> {
  final _addressController = TextEditingController();
  AppLocalizations locale;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    locale = AppLocalizations.of(context);
  }

  @override
  Widget build(BuildContext context) {
    final model = context.watch<AccountModel>();

    return Container(
      padding: EdgeInsets.all(16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(locale.accountLoggedIn, style: TextStyle(fontSize: 16, color: Colors.black45)),
          Text(model.user.email, style: TextStyle(fontSize: 20)),
          SizedBox(height: 16),
          _addressForm(context),
          SizedBox(height: 16),
          _ordersHistory(),
        ],
      ),
    );
  }

  Widget _addressForm(BuildContext context) {
    final model = context.watch<AccountModel>();
    _addressController.value = TextEditingValue(text: model.address);

    return Form(
      child: Focus(
        child: TextFormField(
          controller: _addressController,
          minLines: 2,
          maxLines: 4,
          decoration: InputDecoration(labelText: locale.accountAddress),
          validator: (value) {
            return null;
          },
        ),
        onFocusChange: (hasFocus) {
          if (!hasFocus) {
            final address = _addressController.value.text;
            model.saveAddress(address);
          }
        },
      ),
    );
  }

  Widget _ordersHistory() {
    final model = context.watch<AccountModel>();

    return Expanded(
      child: Card(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(model.orders.isNotEmpty ? locale.accountOrders : locale.accountNoOrders),
              SizedBox(height: 16,),
              Expanded(
                child: ListView.builder(
                  itemCount: model.orders.length,
                  itemBuilder: (context, index) {
                    final order = model.orders[index];
                    return ListTile(
                      title: Text("Order ID: " + order.id),
                      subtitle: Text(order.cartItems.map((e) => e.item.title).join(',') + '\n' + formatDate(context, order.timestamp)),
                      trailing: Text(order.orderStatus),
                    );
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
