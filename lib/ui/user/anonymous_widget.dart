import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shoppy_template/localization.dart';

import 'account_model.dart';

class AnonymousWidget extends StatefulWidget {
  @override
  _AnonymousWidgetState createState() => _AnonymousWidgetState();
}

class _AnonymousWidgetState extends State<AnonymousWidget> {
  final _passwordNode = FocusNode();
  final _formKey = GlobalKey<FormState>();
  Map<ValidationResult, String> emailErrors;
  Map<ValidationResult, String> passwordErrors;
  AppLocalizations locale;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    locale = AppLocalizations.of(context);

    emailErrors = {
      ValidationResult.Ok: null,
      ValidationResult.EmptyEmail: locale.errorEmailEmpty,
      ValidationResult.WrongEmail: locale.errorWrongEmail,
    };
    emailErrors = {
      ValidationResult.Ok: null,
      ValidationResult.EmptyPassword: locale.errorPasswordEmpty,
    };
  }

  @override
  Widget build(BuildContext context) {
    final model = context.watch<AccountModel>();

    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Form(
        key: _formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(locale.notLoggedIn,
                style: TextStyle(fontSize: 24)),
            TextFormField(
              autofocus: true,
              textInputAction: TextInputAction.next,
              keyboardType: TextInputType.emailAddress,
              decoration: InputDecoration(labelText: locale.accountEmail),
              onFieldSubmitted: (value) {
                FocusScope.of(context).requestFocus(_passwordNode);
              },
              validator: (value) {
                final validationResult = model.validateEmail(value);
                return emailErrors[validationResult];
              },
            ),
            TextFormField(
              focusNode: _passwordNode,
              obscureText: true,
              decoration: InputDecoration(labelText: locale.accountPassword),
              validator: (value) {
                final validationResult = model.validatePassword(value);
                return passwordErrors[validationResult];
              },
            ),
            Padding(
              padding: EdgeInsets.only(top: 16.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  RaisedButton(
                    child: Text(locale.accountLogin),
                    onPressed: () async {
                      if (_formKey.currentState.validate()) {
                        final isSuccess = await model.login();
                        if (!isSuccess) {
                            Scaffold.of(context)
                              ..hideCurrentSnackBar()
                              ..showSnackBar(SnackBar(content: Text(model.error)));
                        }
                      }
                    },
                  ),
                  RaisedButton(
                    child: Text(locale.accountRegister),
                    onPressed: () async {
                      if (_formKey.currentState.validate()) {
                        final isSuccess = await model.register();
                        if (!isSuccess) {
                          Scaffold.of(context)
                            ..hideCurrentSnackBar()
                            ..showSnackBar(SnackBar(content: Text(model.error)));
                        }
                      }
                    },
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}