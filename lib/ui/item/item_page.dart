import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shoppy_template/model/dto/item.dart';
import 'package:shoppy_template/ui/cart/cart_model.dart';
import 'package:shoppy_template/ui/home/home_model.dart';
import 'package:shoppy_template/widgets/like_widget.dart';

class ItemPage extends StatefulWidget {
  final ItemPageNavParam _params;

  ItemPage(this._params);

  @override
  _ItemPageState createState() => _ItemPageState();
}

class _ItemPageState extends State<ItemPage> {
  final cornerRadius = Radius.circular(8);

  int _galleryPosition = 0;

  @override
  Widget build(BuildContext context) {
    final item = widget._params.item;
    final tag = widget._params.isBottom ? item.imagesUrls.first : 'photo';

    return Scaffold(
      body: ListView(children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            _buildGallery(item, tag),
            _buildText(item),
          ],
        ),
      ]),
    );
  }

  Widget _buildGallery(Item item, String heroTag) {
    return Container(
      height: 400,
      width: double.infinity,
      child: Stack(
        children: [
          Padding(
            padding: EdgeInsets.only(bottom: 24),
            child: Hero(
              tag: heroTag,
              child: PageView.builder(
                itemCount: item.imagesUrls.length,
                itemBuilder: (context, index) => Image.network(
                  item.imagesUrls[index],
                  fit: BoxFit.cover,
                ),
                onPageChanged: (value) {
                  setState(() {
                    _galleryPosition = value;
                  });
                },
              ),
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Padding(
              padding: EdgeInsets.only(bottom: 24),
              child: _buildPageIndicator(_galleryPosition + 1),
            ),
          ),
          Align(
            alignment: Alignment.bottomLeft,
            child: Padding(
              padding: EdgeInsets.only(bottom: 24),
              child: Consumer<HomeModel>(
                builder: (context, model, child) => LikeWidget(
                  model.isFavorite(item),
                  onPressed: () => model.favorite(item),
                ),
              ),
            ),
          ),
          Align(
            alignment: Alignment.bottomRight,
            child: Padding(
              padding: EdgeInsets.only(right: 24),
              child: FloatingActionButton(
                onPressed: () {
                  final model = context.read<CartModel>();
                  model.add(item);
                },
                heroTag: 'fabBasket',
                child: Icon(Icons.shopping_basket),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _buildPageIndicator(int page) {
    return ClipRRect(
      borderRadius:
          BorderRadius.only(topLeft: cornerRadius, topRight: cornerRadius),
      child: Container(
        child: Text("$page / ${widget._params.item.imagesUrls.length}"),
        color: Colors.white,
        padding: EdgeInsets.all(8),
      ),
    );
  }

  Widget _buildText(Item item) {
    return Padding(
      padding: EdgeInsets.all(16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                item.title,
                style: TextStyle(fontSize: 24),
              ),
              Text(
                "\$${item.price}",
                style: TextStyle(fontSize: 24),
              )
            ],
          ),
          Text(item.brand,
              style: TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.w300,
                  color: Colors.grey)),
          SizedBox(height: 20),
          Text(
            item.description,
            style: TextStyle(fontSize: 18, fontWeight: FontWeight.w300),
          ),
        ],
      ),
    );
  }
}

class ItemPageNavParam {
  Item item;
  bool isBottom;

  ItemPageNavParam(this.item, {this.isBottom = false});
}
