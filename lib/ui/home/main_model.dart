import 'package:flutter/foundation.dart';

import 'main_screen.dart';

class MainModel extends ChangeNotifier {
  int _bottomNavSelectedIndex = 0;
  int get bottomNavSelectedIndex => _bottomNavSelectedIndex;
  set bottomNavSelectedIndex(int value) {
    _bottomNavSelectedIndex = value;
    notifyListeners();
  }

  String searchCategory;

  void search(String category) {
    _bottomNavSelectedIndex = BottomNavigationItems.search;
    searchCategory = category;
    notifyListeners();
  }
}