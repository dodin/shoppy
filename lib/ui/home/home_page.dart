import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shoppy_template/widgets/categories_widget.dart';
import 'package:shoppy_template/widgets/simple_top_bar.dart';

import 'carousel_widget.dart';
import 'home_model.dart';
import 'recommendations_widget.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: move text to localizations
    return Consumer<HomeModel>(
      builder: (context, model, child) => Container(
        padding: EdgeInsets.all(16),
        child: ListView(
          children: [
            SimpleTopBar(title: 'New Collection'),
            SizedBox(height: 20),
            CarouselWidget(),
            SizedBox(height: 20),
            Container(height: 250, child: RecommendationsWidget()),
            SizedBox(height: 20),
            Container(height: 100, child: CategoriesWidget(model.categories)),
          ],
        ),
      ),
    );
  }
}
