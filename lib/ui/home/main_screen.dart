import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shoppy_template/ui/cart/cart_model.dart';
import 'package:shoppy_template/ui/cart/cart_page.dart';
import 'package:shoppy_template/ui/favorites/favorites_page.dart';
import 'package:shoppy_template/ui/home/main_model.dart';
import 'package:shoppy_template/ui/home/search_page.dart';
import 'package:shoppy_template/ui/not_implemented_page.dart';

import 'home_page.dart';

class MainScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => MainModel(),
      child: Consumer<MainModel>(
        builder: (context, model, child) => Scaffold(
          body: SafeArea(child: _getScreen(model.bottomNavSelectedIndex)),
          bottomNavigationBar: BottomNavigationBar(
            type: BottomNavigationBarType.fixed,
            currentIndex: model.bottomNavSelectedIndex,
            onTap: (index) => _updateTabIndex(context, index),
            items: [
              BottomNavigationBarItem(
                icon: Icon(Icons.home),
                title: SizedBox.shrink(),
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.favorite),
                title: SizedBox.shrink(),
              ),
              BottomNavigationBarItem(
                icon: Consumer<CartModel>(
                  builder: (context, cartModel, child) => Badge(
                    showBadge: !cartModel.isEmpty,
                    badgeContent: Text(cartModel.count.toString(), style: TextStyle(color: Colors.white)),
                    child: Icon(Icons.shopping_cart)
                  ),
                ),
                title: SizedBox.shrink(),
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.search),
                title: SizedBox.shrink(),
              )
            ],
          ),
        ),
      ),
    );
  }

  void _updateTabIndex(BuildContext context, int index) {
    final model = context.read<MainModel>();
    model.bottomNavSelectedIndex = index;
  }

  Widget _getScreen(int index) {
    switch(index) {
      case BottomNavigationItems.home:
        return HomePage();
      case BottomNavigationItems.cart:
        return CartPage();
      case BottomNavigationItems.favorites:
        return FavoritesPage();
      case BottomNavigationItems.search:
        return SearchPage();
      default:
        return NotImplementedPage();
    }
  }
}

class BottomNavigationItems {
  static const home = 0;
  static const favorites = 1;
  static const cart = 2;
  static const search = 3;
}