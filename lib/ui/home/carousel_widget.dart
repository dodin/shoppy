import 'package:flutter/material.dart';
import 'package:page_view_indicators/circle_page_indicator.dart';
import 'package:provider/provider.dart';
import 'package:shoppy_template/model/dto/item.dart';
import 'package:shoppy_template/ui/cart/cart_model.dart';
import 'package:shoppy_template/ui/item/item_page.dart';
import 'package:shoppy_template/widgets/like_widget.dart';

import 'home_model.dart';

class CarouselWidget extends StatefulWidget {
  @override
  _CarouselWidgetState createState() => _CarouselWidgetState();
}

class _CarouselWidgetState extends State<CarouselWidget> {
  final _currentPageNotifier = ValueNotifier<int>(0);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Consumer<HomeModel>(
            builder: (context, model, child) {
              var items =
                  model.items.where((element) => element.isCarousel).toList();
              return items.isEmpty
                  ? SizedBox.shrink()
                  : Column(
                      children: [
                        Container(
                          height: 320,
                          child: PageView.builder(
                            scrollDirection: Axis.horizontal,
                            itemCount: items.length,
                            itemBuilder: (context, position) =>
                                _buildGalleryItem(
                                    model, items[position], items.length),
                            onPageChanged: (index) {
                              _currentPageNotifier.value = index;
                            },
                          ),
                        ),
                        CirclePageIndicator(
                          itemCount: items.length,
                          currentPageNotifier: _currentPageNotifier,
                        )
                      ],
                    );
            },
          )
        ],
      ),
    );
  }

  Widget _buildGalleryItem(HomeModel model, Item item, int total) {
    final galleryItemPhoto = Stack(
      children: [
        Padding(
          padding: EdgeInsets.only(bottom: 20.0),
          child: Container(
            width: double.infinity,
            height: 240,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(20)),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  spreadRadius: 0,
                  blurRadius: 10,
                  offset: Offset(0, 10),
                )
              ],
            ),
            child: ClipRRect(
              borderRadius: BorderRadius.all(Radius.circular(20)),
              child: GestureDetector(
                onTap: () => Navigator.pushNamed(context, '/item',
                    arguments: ItemPageNavParam(item)),
                child: Hero(
                  tag: 'photo',
                  child:
                      Image.network(item.imagesUrls.first, fit: BoxFit.cover),
                ),
              ),
            ),
          ),
        ),
        Align(
          alignment: Alignment.topRight,
          child: LikeWidget(
            model.isFavorite(item),
            onPressed: () => model.favorite(item),
          ),
        ),
        Positioned(
          bottom: 0,
          right: 16,
          child: FloatingActionButton(
            child: Icon(Icons.shopping_basket),
            onPressed: () {
              final cartModel = context.read<CartModel>();
              cartModel.add(item);
            },
          ),
        )
      ],
    );

    final galleryItemText = Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                item.brand + ' ' + item.title,
                style: TextStyle(fontSize: 28),
                overflow: TextOverflow.ellipsis,
              ),
              Text(
                item.description,
                style: TextStyle(
                  color: Colors.black54,
                  fontWeight: FontWeight.w400,
                ),
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              )
            ],
          ),
        ),
        Column(
          children: [
            Text(
              "${item.price}\$",
              style: TextStyle(fontSize: 28),
            ),
          ],
        )
      ],
    );

    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          galleryItemPhoto,
          galleryItemText,
        ],
      ),
    );
  }
}
