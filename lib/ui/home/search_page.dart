import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';
import 'package:shoppy_template/model/dto/item.dart';
import 'package:shoppy_template/ui/home/home_model.dart';
import 'package:shoppy_template/ui/item/item_page.dart';
import 'package:shoppy_template/widgets/categories_widget.dart';

import 'main_model.dart';

class SearchPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final mainModel = context.watch<MainModel>();
    final homeModel = context.watch<HomeModel>();
    final category = mainModel.searchCategory;
    final items = homeModel.items
        .where((element) => element.category == category)
        .toList();

    return Container(
      padding: EdgeInsets.all(16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
              height: 100,
              child: CategoriesWidget(homeModel.categories,
                  highlightSelected: true)),
          mainModel.searchCategory == null
              ? SizedBox.shrink()
              : ListView.builder(
                  shrinkWrap: true,
                  itemCount: items.length,
                  itemBuilder: (context, index) =>
                      _buildItem(context, homeModel, items[index]),
                )
        ],
      ),
    );
  }

  Widget _buildItem(BuildContext context, HomeModel homeModel, Item item) {
    final isFavorite = homeModel.isFavorite(item);
    return ListTile(
      onTap: () => Navigator.pushNamed(context, '/item',
          arguments: ItemPageNavParam(item, isBottom: true)),
      title: Text(item.title),
      subtitle: Text(item.brand),
      leading: Hero(
          tag: item.imagesUrls.first,
          child: Image.network(item.imagesUrls.first, width: 100)),
      trailing: IconButton(
        icon: isFavorite
            ? Icon(Icons.favorite, color: Colors.red)
            : Icon(Icons.favorite_border, color: Colors.black),
        onPressed: () => homeModel.favorite(item),
      ),
    );
  }
}
