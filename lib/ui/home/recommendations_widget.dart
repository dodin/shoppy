import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shoppy_template/model/dto/item.dart';
import 'package:shoppy_template/ui/item/item_page.dart';
import 'package:shoppy_template/widgets/like_widget.dart';

import 'home_model.dart';

class RecommendationsWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Consumer<HomeModel>(
      builder: (context, model, child) {
        var recommend = [...model.items];
        recommend.shuffle();
        recommend = recommend.take(6).toList();

        return ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: recommend.length,
          itemBuilder: (_, index) =>
              _buildGalleryItem(context, model, recommend[index]),
        );
      },
    );
  }

  Widget _buildGalleryItem(BuildContext context, HomeModel model, Item item) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16),
      child: Column(
        children: [
          ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(20)),
            child: Container(
              color: Colors.grey,
              child: Stack(
                children: [
                  GestureDetector(
                    onTap: () => Navigator.pushNamed(context, '/item',
                        arguments: ItemPageNavParam(item, isBottom: true)),
                    child: Hero(
                      tag: item.imagesUrls.first,
                      child: Image.network(
                        item.imagesUrls.first,
                        height: 200,
                        width: 200,
                      ),
                    ),
                  ),
                  Positioned(
                    right: 1,
                    child: LikeWidget(
                      model.isFavorite(item),
                      onPressed: () => model.favorite(item),
                    ),
                  )
                ],
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 8),
            child: Text(
              item.title,
              overflow: TextOverflow.ellipsis,
            ),
          )
        ],
      ),
    );
  }
}
