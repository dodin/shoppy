import 'package:flutter/foundation.dart';
import 'package:get_it/get_it.dart';
import 'package:shoppy_template/model/dto/item.dart';
import 'package:shoppy_template/model/repository.dart';

class HomeModel extends ChangeNotifier {
  List<Item> _items = [];
  List<Item> get items => _items;
  Set<String> _categories = {};
  Set<String> get categories => _categories;

  Repository _repository = GetIt.I<Repository>();

  HomeModel() {
    load();
  }

  void load() async {
    _items = await _repository.loadData();
    await _repository.loadFavorites();
    _categories = await _repository.loadCategories();

    notifyListeners();
  }

  void favorite(Item item) async {
    print("favorite " + item.sku);
    await _repository.toggleFavorite(item);

    notifyListeners();
  }

  bool isFavorite(Item item) {
    return _repository.isFavorite(item);
  }
}