
import 'package:shoppy_template/model/dto/item.dart';

class CartItem {
  Item item;
  int count;

  CartItem(this.item, this.count);
}