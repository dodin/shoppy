import 'package:flutter/foundation.dart';
import 'package:get_it/get_it.dart';
import 'package:shoppy_template/model/cart.dart';
import 'package:shoppy_template/model/dto/item.dart';
import 'package:shoppy_template/model/repository.dart';

import 'cart_item.dart';

// TODO: fix cart
class CartModel extends ChangeNotifier {
  Repository _repository = GetIt.I<Repository>();

  Cart _cart = Cart();
  List<CartItem> get items => _cart.items;
  double get totalPrice => _cart.totalPrice;
  bool get isEmpty => items.isEmpty;
  int get count => items.fold(0, (previousValue, element) => previousValue + element.count);

  CartModel() {
    load();
  }

  void load() async {
    final itemsInCart = await _repository.loadCart();
    _cart = Cart(items: itemsInCart);
    notifyListeners();
  }

  void delete(int position) async {
    final items = await _repository.deleteFromCart(_cart.items[position].item);
    _cart = Cart(items: items);
    notifyListeners();
  }

  void add(Item item) async {
    final items = await _repository.addToCart(item);
    _cart = Cart(items: items);
    notifyListeners();
  }

  void clear() {
    _repository.clearCart();
    _cart = Cart();
    notifyListeners();
  }
}