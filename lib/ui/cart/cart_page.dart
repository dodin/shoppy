import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shoppy_template/localization.dart';
import 'package:shoppy_template/widgets/simple_top_bar.dart';

import 'cart_model.dart';

class CartPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.all(16),
        child: Column(
          children: [
            SimpleTopBar(title: AppLocalizations.of(context).cartTitle),
            Expanded(child: _buildList()),
            _buildTotalPrice(context),
            MaterialButton(
              minWidth: double.infinity,
              color: Colors.red,
              child: Text(AppLocalizations.of(context).cartCheckout,
                  style: TextStyle(color: Colors.white)),
              onPressed: () => Navigator.pushNamed(context, '/checkout'),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildList() {
    return Consumer<CartModel>(
      builder: (context, model, child) {
        return model.items.isEmpty
            ? Container(
                alignment: Alignment.topLeft,
                child: Text(AppLocalizations.of(context).cartEmpty),
              )
            : ListView.builder(
                itemCount: model.items.length,
                itemBuilder: (_, position) {
                  var item = model.items[position];
                  return Dismissible(
                    key: UniqueKey(),
                    onDismissed: (direction) async {
                      model.delete(position);
                    },
                    direction: DismissDirection.endToStart,
                    background: Container(
                      alignment: Alignment.centerRight,
                      child: Icon(
                        Icons.delete,
                        color: Colors.white,
                      ),
                      color: Colors.red,
                    ),
                    child: ListTile(
                      leading: Image.network(item.item.imagesUrls.first),
                      title: Text(item.item.title),
                      trailing: Text("\$${item.item.price} x ${item.count}"),
                    ),
                  );
                },
              );
      },
    );
  }

  Widget _buildTotalPrice(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(AppLocalizations.of(context).cartTotal),
        Consumer<CartModel>(
          builder: (context, model, child) => Text('\$${model.totalPrice}'),
        ),
      ],
    );
  }
}
