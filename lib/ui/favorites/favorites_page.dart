import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shoppy_template/localization.dart';
import 'package:shoppy_template/model/dto/item.dart';
import 'package:shoppy_template/ui/item/item_page.dart';
import 'package:shoppy_template/widgets/simple_top_bar.dart';

import 'favorites_model.dart';

class FavoritesPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ChangeNotifierProvider(
        create: (_) => FavoritesModel(),
        child: Container(
          padding: EdgeInsets.all(16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SimpleTopBar(title: AppLocalizations.of(context).fav),
              Expanded(
                child: Consumer<FavoritesModel>(
                  builder: (context, model, child) => model.items.isNotEmpty
                      ? ListView.builder(
                          itemCount: model.items.length,
                          itemBuilder: (context, index) =>
                              _buildItem(context, model.items[index]),
                        )
                      : Text(AppLocalizations.of(context).favEmpty),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildItem(BuildContext context, Item item) {
    final model = context.read<FavoritesModel>();
    return ListTile(
      onTap: () => Navigator.pushNamed(context, '/item', arguments: ItemPageNavParam(item, isBottom: true)),
      title: Text(item.title),
      subtitle: Text(item.brand),
      leading: Hero(tag: item.imagesUrls.first, child: Image.network(item.imagesUrls.first, width: 100)),
      trailing: IconButton(
        icon: Icon(Icons.favorite, color: Colors.red),
        onPressed: () => model.remove(item),
      ),
    );
  }
}
