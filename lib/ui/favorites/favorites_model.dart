import 'package:flutter/foundation.dart';
import 'package:get_it/get_it.dart';
import 'package:shoppy_template/model/dto/item.dart';
import 'package:shoppy_template/model/repository.dart';

class FavoritesModel extends ChangeNotifier {
  Repository _repository = GetIt.I<Repository>();

  List<Item> _items = [];
  List<Item> get items => _items;

  FavoritesModel() {
    load();
  }

  void load() async {
    _items = await _repository.loadFavorites();
    notifyListeners();
  }

  void remove(Item item) async {
    _items.remove(item);
    await _repository.toggleFavorite(item);
    notifyListeners();
  }
}