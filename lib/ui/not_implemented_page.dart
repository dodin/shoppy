import 'package:flutter/material.dart';

class NotImplementedPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(child: Text('Not implemented'));
  }
}
