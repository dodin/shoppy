import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';

String formatDate(BuildContext context, String raw) {
  final locale = Localizations.localeOf(context);

  final DateFormat formatter = DateFormat('dd MMM yyyy', locale.languageCode);
  final date = DateTime.parse(raw);
  return formatter.format(date);
}