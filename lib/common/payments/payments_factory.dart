import 'cash.dart';
import 'credit_card.dart';
import 'payment_method.dart';
import 'paypal.dart';

class PaymentsFactory {
  static final methods = {
    'PayPal': PayPal(),
    'Credit card': CreditCard(),
    'Cash': Cash(),
  };

  static PaymentMethod getPayment(String key) {
    return methods[key];
  }
}