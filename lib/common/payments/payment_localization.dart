import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class PaymentLocalizations {
  PaymentLocalizations(this.locale);

  final Locale locale;

  static PaymentLocalizations of(BuildContext context) {
    return Localizations.of<PaymentLocalizations>(context, PaymentLocalizations);
  }

  static Map<String, Map<String, String>> _localizedValues = {
    'en': {
      'PayPal': 'PayPal',
      'Credit card': 'Credit card',
      'Cash': 'Cash',
    },
    'ru': {
      'PayPal': 'PayPal',
      'Credit card': 'Картой',
      'Cash': 'Наличка',
    },
  };

  String get(String key) {
    return _localizedValues[locale.languageCode][key];
  }
}

class PaymentLocalizationsDelegate extends LocalizationsDelegate<PaymentLocalizations> {
  const PaymentLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) => ['en', 'ru'].contains(locale.languageCode);

  @override
  Future<PaymentLocalizations> load(Locale locale) {
    return SynchronousFuture<PaymentLocalizations>(PaymentLocalizations(locale));
  }

  @override
  bool shouldReload(PaymentLocalizationsDelegate old) => false;
}