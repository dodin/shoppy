import 'package:flutter/material.dart';
import 'package:shoppy_template/common/payments/payment_localization.dart';
import 'package:shoppy_template/common/payments/payment_method.dart';

import 'paypal/paypal_payment_widget.dart';

class PayPal extends PaymentMethod {
  @override
  String name() => 'PayPal';


  @override
  String localizedName(BuildContext context) {
    return PaymentLocalizations.of(context).get(name());
  }

  @override
  Widget getForm(BuildContext context) {
    return SizedBox.shrink();
  }

  @override
  void execute(BuildContext context, Function callback) {
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (BuildContext context) => PaypalPayment(
          onFinish: (number) async {
            // payment done
            callback?.call(number);
            print('order id: '+number);
          },
        ),
      ),
    );
  }
}