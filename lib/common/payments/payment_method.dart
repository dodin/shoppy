import 'package:flutter/widgets.dart';

import 'payment_localization.dart';

class PaymentMethod {
  // ignore: missing_return
  String name() {}

  // ignore: missing_return
  String localizedName(BuildContext context) {
    return PaymentLocalizations.of(context).get(name());
  }

  // ignore: missing_return
  Widget getForm(BuildContext context) {}

  // ignore: missing_return
  void execute(BuildContext context, Function callback) {}
}

class PaymentData {
  double totalAmount;
  List<String> items;
}