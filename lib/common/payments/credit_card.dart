import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:shoppy_template/common/payments/payment_localization.dart';
import 'package:shoppy_template/common/payments/payment_method.dart';

class CreditCard extends PaymentMethod {
  @override
  String name() => 'Credit card';


  @override
  String localizedName(BuildContext context) {
    return PaymentLocalizations.of(context).get(name());
  }

  @override
  Widget getForm(BuildContext context) {
    final hint = name();
    return Container(
      child: TextFormField(
        decoration: InputDecoration(
          hintText: hint,
          labelText: hint,
        ),
        validator: (value) {
          if (EmailValidator.validate(value)) {
            return null;
          }

          return 'Error';
        },
      ),
    );
  }

  @override
  void execute(BuildContext context, Function callback) {
    callback.call(null);
  }
}