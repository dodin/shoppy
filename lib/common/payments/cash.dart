import 'package:flutter/material.dart';
import 'package:shoppy_template/common/payments/payment_localization.dart';
import 'package:shoppy_template/common/payments/payment_method.dart';

class Cash extends PaymentMethod {
  @override
  String name() => 'Cash';

  @override
  String localizedName(BuildContext context) {
    return PaymentLocalizations.of(context).get(name());
  }

  @override
  Widget getForm(BuildContext context) {
    return SizedBox.shrink();
  }

  @override
  void execute(BuildContext context, Function callback) {
    callback.call(null);
  }
}