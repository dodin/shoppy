import 'package:get_it/get_it.dart';
import 'package:shoppy_template/model/gsheets_repository.dart';
import 'package:shoppy_template/model/repository.dart';

import 'model/authentication.dart';

GetIt locator = GetIt.instance;

void setupLocator() {
  locator.registerFactory<Authentication>(() => Authentication());
  locator.registerSingleton<Repository>(GSheetsRepository());
}