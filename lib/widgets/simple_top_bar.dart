import 'package:flutter/material.dart';

import '../router.dart';
import 'circle_button.dart';

/// Simple top bar widget consists of title and account button.
/// It has fixed top padding.
class SimpleTopBar extends StatelessWidget {
  final double topPadding;
  final String title;

  SimpleTopBar({this.title, this.topPadding = 16.0});

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.only(top: topPadding),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(title, style: theme.textTheme.headline4),
              CircleButton(
                Icons.account_circle,
                onPressed: () => Navigator.pushNamed(context, Routes.accountPage),
              )
            ],
          ),
        ),
        SizedBox(height: 8),
        Divider(),
      ],
    );
  }
}
