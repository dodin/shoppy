import 'package:flutter/material.dart';

class LikeWidget extends StatelessWidget {
  final bool liked;
  final Function onPressed;

  LikeWidget(this.liked, {this.onPressed});

  @override
  Widget build(BuildContext context) {
    return IconButton(
      onPressed: () => onPressed(),
      icon: liked
          ? Icon(Icons.favorite, color: Colors.red)
          : ClipRRect(
              borderRadius: BorderRadius.all(Radius.circular(16)),
              child: Container(
                  color: Colors.black12,
                  alignment: Alignment.center,
                  padding: EdgeInsets.all(2),
                  child: Icon(Icons.favorite_border, color: Colors.white))),
    );
  }
}
