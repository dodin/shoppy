import 'package:flutter/material.dart';
import 'package:shoppy_template/ui/home/main_model.dart';
import 'package:provider/provider.dart';

class CategoriesWidget extends StatelessWidget {
  final Set<String> _categories;
  final bool highlightSelected;

  CategoriesWidget(this._categories, {this.highlightSelected = false});

  @override
  Widget build(BuildContext context) {
    return _categories.isEmpty
        ? SizedBox.shrink()
        : ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: _categories.length,
            itemBuilder: (context, index) {
              MainModel mainModel = context.read<MainModel>();
              var category = _categories.elementAt(index);
              final isSelected = category == mainModel.searchCategory;
              return MaterialButton(
                child: Text(category,
                    style: TextStyle(
                        fontSize: 24,
                        color: highlightSelected && isSelected
                            ? Colors.blue
                            : Colors.black)),
                onPressed: () {
                  mainModel.search(category);
                },
              );
            },
          );
  }
}
