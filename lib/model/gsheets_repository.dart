import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:get_it/get_it.dart';
import 'package:gsheets/gsheets.dart';
import 'package:shoppy_template/model/repository.dart';

import 'authentication.dart';
import 'dto/item.dart';
import 'dto/order.dart';
import 'dto/profile.dart';

// TODO: Move credentials outside the code
const _credentials = r'''
{
  "type": "service_account",
  "project_id": "training-chat",
  "private_key_id": "d997c0964c5fa18d4696e7eb589f7eb9672180ad",
  "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQDVQX3Dy0P9jTHl\n4YREMoF2428S4kgkBVBWHU/SmnOLRNRVzkGpZnHavwZB1GTSOUjTCW8Z+e9qHnL6\nHZTScWIkdX+VHm9IMgMJcebYmPtfxqKERNaZcgrNXgjYXOUAcwzJTrwKjIgViX3X\nSopuR6BKGa6uBp/HkkTSaoWPJtf6CVhTGD8W28ocRixNrV6+ObegFgbgJ29ZXxiP\nee6v+ZY2h7+gKIvW/T3hx2y17cICxsqWBTuqH8M/nXLFbreJrPlK5zjZlJtDlTg6\nQCYSuhC0rf/h4yV6HW21/qn6g+9gQ3yVrHnEhF/B675fwaw9HA9WSLQqT8ERDXNg\nzZnX6GOPAgMBAAECggEAR97disoLbP1T7ryVPgKjfTTjL0MRuwoLbCpOx5SEElCt\nGwn7OSMsheNRMYhqPitOLsmEJwYDuQjwCPlFObM6tzzeNZxfNr6cJWe8vAy60yl3\nhgpJJMe5ekMRyhFsoD0l9wZQ+2EnT9LZlWAqzUXYcIC9TSswWCskhKGfx4H40ewM\neuA7LM0aw84s3h6vU3jO+N00d5Rdy4MDnoDFs8aZpt5jSq4YO3lCbm48UJAOvfjc\n0ODiqAYHJLrq3OlHsDTvZ8ufUXiulAu2raA/ZQPKoQ8+Ybugvbqd8KHhilsg9r4q\nxEiI53d64cFfY3uVJ73juizb/0h2W95VTjN9/QSL8QKBgQD6OIbCegAe+mzl2kM+\nn8NMkKN3ei59rGWwhu0oM1Af+RWzvbsWB1OfJkk2hTY71IhVPGz6W1b/lQh324o2\nH8RVSb+wvUdvuYL1MvVlmkRp2lPzLAwEc7AZb28AIYAZU6skilUtE44BFeMFbOMv\nlQy2rKgYNHAg4YwcX+mbeMlOZwKBgQDaLmcuRZ/gwzxmtDAHUD4JZwAToCWw0/18\nFzIfmaV4Q09RwFvJIP/L9lSU2jxTK54VLL4sUYAFLcZBlUS8F0XMV7lIAJOvr6T6\nWhh4zaLe3C89n2OujYuYjXNwQ2KVwg4NKg1rXBqKMRQsFzeiRoxeMMcZcBcgFU14\nYMURYr44mQKBgHwEPDZzhU/VStf5f86wHHn8TijPc+9x3MzLcwuzswjFyc3WQb2/\nT+DXdv4AtEqfIhss4c6MKuhuE05kj3IX+JVVAh81EQz84z1oTlxXQ4U7YtRphmNG\ne4d/xYJJGFZ1M6PetCytPnqnQGfV+hwBQdXgaUfl5ZVGdM0zdHNMQGoNAoGAOP3M\nCAM52v1EptNEhHSKb+xO0dGQZttJKIPqnOSGm56BdqpXj3cmOkHzIBrNjTAkkAVq\n+6qiVTf6xKDYHoW5vsHfbRZxfX/VCWxFXjDg7XP4hxE+eZcRstyoHdgMRXA2OWwV\nq0iYyiY9zbKW3rWs7uGw8E0eTUwjO2GTuJHhfGkCgYAtbZODZv8qfiKMaPlibHX1\np34n0YuZEGv6wCGsM9plD7wNHjZT0fMuf38iDH1Zyle6KK1AdlhMRQ/tqFjl1TXH\nEZCdb5FWVNTwY/pZysTGgBlAOeojj/nLaJ8KQjWdTD6CqM0MwoWwcRxRqWEGYMJK\npk78yTMjXtjA6XLu1iHVfg==\n-----END PRIVATE KEY-----\n",
  "client_email": "training-chat@appspot.gserviceaccount.com",
  "client_id": "",
  "auth_uri": "https://accounts.google.com/o/oauth2/auth",
  "token_uri": "https://oauth2.googleapis.com/token",
  "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
  "client_x509_cert_url": ""
}
''';

class GSheetsRepository implements Repository {
  final spreedSheetId = '1LPOQiDoWsASOtgapoYGbuaywon0TmeY3BQIUY7JxOhU';
  final dataWorksheetTitle = 'data';
  final shippingWorksheetTitle = 'shipping';
  final paymentsWorksheetTitle = 'payments';

  FirebaseFirestore _firestore = FirebaseFirestore.instance;
  GSheets _gsheets;
  Authentication _auth;

  List<Item> _data = [];
  Set<String> _favorites = {};
  List<Item> _cart = [];

  GSheetsRepository() {
    _gsheets = GSheets(_credentials);
    _auth = GetIt.I<Authentication>();
  }

  @override
  Future<List<Item>> loadCart() async {
    return _cart;
  }

  @override
  Future<List<Item>> addToCart(Item item) async {
   _cart.add(item);
   return _cart;
  }

  @override
  Future<List<Item>> loadData() async {
    if (_data.isNotEmpty) {
      return _data;
    }

    var sheet = await _getWorksheet(dataWorksheetTitle);
    // Skip header
    var data = await sheet.cells.allRows(fromRow: 2);

    var result = data
        .map((e) => Item(
              e[GSheetsItemFields.SKU].value,
              e[GSheetsItemFields.TITLE].value,
              e[GSheetsItemFields.BRAND].value,
              e[GSheetsItemFields.DESCRIPTION].value,
              e[GSheetsItemFields.PRICE].value,
              e[GSheetsItemFields.IMAGES].value.split('\n'),
              isCarousel:
                  _strToBool(e[GSheetsItemFields.IS_GALLERY].value),
              category: e[GSheetsItemFields.CATEGORY].value,
            ))
        .toList();

    _data = result;
    return result;
  }

  @override
  Future<List<Item>> loadFavorites() async {
    if (_favorites.isNotEmpty) {
      return _buildFavorites();
    }

    final user = _auth.user;
    if (user == null) {
      return Future.value([]);
    }

    final documentPath = 'users/${user.uid}/favorites';
    try {
      final fav = await _firestore.collection(documentPath).get();
      if (fav.size > 0) {
        _favorites = fav.docs.map((e) => e.id).toSet();
        return _buildFavorites();
      }
    } catch (Exception) {}
    return Future.value([]);
  }

  List<Item> _buildFavorites() {
    return _data.where((element) => _favorites.contains(element.sku)).toList();
  }

  @override
  Future<List<Item>> toggleFavorite(Item item) async {
    if (_auth.user == null) {
      await _auth.signInAnonymously();
    }

    return await _toggleFavorite(_auth.user, item);
  }

  Future<List<Item>> _toggleFavorite(User user, Item item) async {
    final documentPath = 'users/${user.uid}/favorites/${item.sku}';
    final fav = await _firestore.doc(documentPath).get();
    if (fav.exists) {
      _firestore.doc(documentPath).delete();
    } else {
      _firestore.doc(documentPath).set({'added': true});
    }

    // TODO: refactor! move to separate local storage class
    // toggle locally
    if (isFavorite(item)) {
      _favorites.removeWhere((element) => element == item.sku);
    } else {
      _favorites.add(item.sku);
    }

    return _buildFavorites();
  }

  @override
  bool isFavorite(Item item) {
    return _favorites.firstWhere((element) => element == item.sku, orElse: () => null) != null;
  }

  @override
  Future<Set<String>> loadCategories() async {
    return _data.map((e) => e.category).toSet();
  }

  @override
  Future<Set<String>> loadShippingMethods() async {
    final shippingSheet = await _getWorksheet(shippingWorksheetTitle);
    final rows = await shippingSheet.cells.allRows();
    final result = rows.map((e) => e[0].value).toSet();
    return result;
  }

  @override
  Future<Set<String>> loadPaymentMethods() async {
    final shippingSheet = await _getWorksheet(paymentsWorksheetTitle);
    final rows = await shippingSheet.cells.allRows();
    final result = rows.map((e) => e[0].value).toSet();
    return result;
  }

  @override
  Future<bool> saveOrder(Order order) async {
    final user = _auth.user;
    if (user == null) {
      return false;
    }

    final documentPath = 'users/${user.uid}/orders';
    try {
      final collection = _firestore.collection(documentPath);
      var orderMap = order.toMap();
      if (order.timestamp == null) {
        orderMap['timestamp'] = FieldValue.serverTimestamp();
      }
      final result = await collection.add(orderMap);
      print("Saved order id: ${result.id}");
    } catch (Exception) {
      return false;
    }

    return true;
  }

  @override
  Future<void> clearCart() async {
     _cart.clear();
  }

  @override
  Future<List<Item>> deleteFromCart(Item item) async {
    _cart.removeWhere((element) => element.sku == item.sku);
    return _cart;
  }


  @override
  Future<void> updateProfile({String address}) async {
    if (address.isEmpty) {
      return;
    }

    final user = _auth.user;
    if (user == null) {
      return;
    }

    final documentPath = 'users/${user.uid}';
    final profile = {
      'address': address,
    };
    return await _firestore.doc(documentPath).set(profile);
  }


  @override
  Future<Profile> loadProfile() async {
    final user = _auth.user;
    if (user == null) {
      return null;
    }

    final documentPath = 'users/${user.uid}';
    final doc = await _firestore.doc(documentPath).get();

    return Profile(doc.get('address'));
  }


  @override
  Future<List<Order>> loadOrderHistory() async {
    final user = _auth.user;
    if (user == null) {
      return null;
    }

    final documentPath = 'users/${user.uid}/orders';
    try {
      final snapshots = await _firestore.collection(documentPath).get();
      final result = snapshots.docs.map((e) {
        final order = Order.fromMap(e.data());
        order.id = e.id;
        return order;
      }).toList();
      return result;
    } catch (e) {
      return [];
    }
  }

  Future<Worksheet> _getWorksheet(String title) async {
    var spreadsheet = await _gsheets
        .spreadsheet(spreedSheetId);
    return spreadsheet.worksheetByTitle(title);
  }

  bool _strToBool(String value) {
    return value.toLowerCase() == 'true';
  }
}

class GSheetsItemFields {
  static const int SKU = 0;
  static const int BRAND = 1;
  static const int TITLE = 2;
  static const int PRICE = 3;
  static const int DESCRIPTION = 4;
  static const int IMAGES = 5;
  static const int IS_GALLERY = 6;
  static const int CATEGORY = 7;
}
