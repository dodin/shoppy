import 'dto/item.dart';
import 'dto/order.dart';
import 'dto/profile.dart';
import 'repository.dart';

class DummyRepository implements Repository {
  @override
  Future<List<Item>> loadData() async {
    return await Future.value(dummyResponse);
  }

  @override
  Future<List<Item>> loadCart() async {
    return await Future.value(dummyResponse.take(2).toList());
  }

  @override
  Future<List<Item>> loadFavorites() async {
    return await Future.value(dummyResponse.take(2).toList());
  }

  @override
  Future<List<Item>> toggleFavorite(Item item) async {
    return await Future.delayed(Duration(milliseconds: 200));
  }

  @override
  bool isFavorite(Item item) {
    return false;
  }

  @override
  Future<List<Item>> addToCart(Item item) async {
    return dummyResponse;
  }

  @override
  Future<Set<String>> loadCategories() async {
    return {'Phones', 'PowerBanks'};
  }

  @override
  Future<Set<String>> loadPaymentMethods() async {
    return {'Credit card'};
  }

  @override
  Future<Set<String>> loadShippingMethods() async {
    return {'Standard', 'Courier'};
  }

  @override
  Future<bool> saveOrder(Order order) async {
    return true;
  }

  @override
  Future<void> clearCart() async {

  }

  @override
  Future<List<Item>> deleteFromCart(Item item) async {
    return dummyResponse;
  }

  @override
  Future<void> updateProfile({String address}) async {

  }

  @override
  Future<Profile> loadProfile()  async {
    return Profile("address");
  }

  @override
  Future<List<Order>> loadOrderHistory() async {
    return [];
  }
}

final dummyResponse = [
  item1,
  item2,
  item3,
  item4,
];

final item1 = Item("001", "Xiaomi PowerBank", "Xiaomi", "This PowerBank can save your life", "20", ["https://n3.sdlcdn.com/imgs/a/r/v/Xiaomi-10400mah-Mi-Powerbank-With-SDL430372534-1-dfe28.jpg"], isCarousel: true);
final item2 = Item("002", "Xiaomi Redmi 8", "Xiaomi", "One of the most popular phones in the World!", "200", ["https://i01.appmifile.com/webfile/globalimg/in/cms/528D4EBF-9DB2-04D7-A1EB-24E4D73E0ABF.jpg"], isCarousel: true);
final item3 = Item("003", "Xiaomi Redmi 9", "Xiaomi", "New version of bestseller is now available", "250", ["https://www.gizmochina.com/wp-content/uploads/2020/03/Redmi-Note-9-Pro-concept-render.jpg", "https://mi-store.pl/pol_pm_Smartfon-Xiaomi-Redmi-Note-9-Pro-6-64GB-Tropical-Green-Mi-Smart-Band-4-772_9.jpg"], isCarousel: true);
final item4 = Item("005", "OnePlus 8", "OnePlus", "New flagship phone from OnePlus", "800", ["https://www.droid-life.com/wp-content/uploads/2020/04/OnePlus-8-Colors.jpg"]);