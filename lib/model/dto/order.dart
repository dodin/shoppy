import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:shoppy_template/model/dto/item.dart';
import 'package:shoppy_template/ui/cart/cart_item.dart';

class Order {
  String id;
  List<CartItem> cartItems;
  double amount;
  String paymentMethod;
  String paymentStatus;
  String paymentDetails;
  String shippingMethod;
  String address;
  String orderStatus;
  String timestamp;

  Order({this.cartItems, this.amount, this.paymentMethod, this.paymentStatus,
      this.paymentDetails, this.shippingMethod, this.address, this.orderStatus, this.timestamp});

  Map<String, dynamic> toMap() {
    return {
      'orderStatus': orderStatus,
      'amount': amount,
      'paymentMethod': paymentMethod,
      'paymentStatus': paymentStatus,
      'paymentDetails': paymentDetails,
      'shippingMethod': shippingMethod,
      'address': address,
      'timestamp': timestamp,
      'cartItems': cartItems.map((e) => {
        'sku': e.item.sku,
        'title': e.item.brand + ' ' + e.item.title,
        'price': e.item.price,
        'count': e.count
      }).toList()
    };
  }

  Order.fromMap(Map<String, dynamic> map) {
    orderStatus = map['orderSnapshot'] ?? 'Pending';
    amount = map['amount'];
    paymentMethod = map['paymentMethod'];
    paymentStatus = map['paymentStatus'];
    paymentDetails = map['paymentDetails'];
    shippingMethod = map['shippingMethod'];
    address = map['address'];
    timestamp = DateTime.fromMillisecondsSinceEpoch((map['timestamp'] as Timestamp).millisecondsSinceEpoch).toLocal().toString();

    cartItems = map['cartItems'].map((e) {
      return CartItem(
          Item(
              e['sku'],
              e['title'],
              '',
              '',
              e['price'],
              []
          ),
          e['count']
      );
    }).cast<CartItem>().toList();
  }
}