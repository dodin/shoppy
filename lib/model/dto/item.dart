class Item {
  final String sku;
  final String brand;
  final String title;
  final String description;
  final String price;
  final List<String> imagesUrls;
  bool isCarousel;
  final String category;

  Item(this.sku, this.title, this.brand, this.description, this.price, this.imagesUrls, {this.isCarousel = false, this.category = ''});
}