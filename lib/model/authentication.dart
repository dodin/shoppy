import 'package:firebase_auth/firebase_auth.dart';

class Authentication {
  FirebaseAuth _auth;
  User _user;

  User get user => _user;

  Authentication() {
    _auth = FirebaseAuth.instance;
    _user = _auth.currentUser;
  }

  Future<User> signInAnonymously() async {
    UserCredential userCredential = await _auth.signInAnonymously();
    _user = userCredential.user;
    return _user;
  }

  Future<AuthResult> signInWithEmail(String email, String password) async {
    try {
      UserCredential userCredential = await _auth.signInWithEmailAndPassword(
          email: email, password: password);
      _user = userCredential.user;
      return AuthResult.success(_user);
    } on FirebaseAuthException catch (e) {
      return AuthResult.error(e);
    }
  }

  Future<AuthResult> registerWithEmail(String email, String password) async {
    try {
      if (_user == null) {
        UserCredential userCredential = await _auth
            .createUserWithEmailAndPassword(email: email, password: password);
        _user = userCredential.user;
      } else {
        final credentials =
            EmailAuthProvider.credential(email: email, password: password);
        _user.linkWithCredential(credentials);
      }

      return AuthResult.success(_user);
    } on FirebaseAuthException catch (e) {
      return AuthResult.error(e);
    }
  }
}

class AuthResult {
  User user;
  FirebaseAuthException e;

  AuthResult.success(this.user);

  AuthResult.error(this.e);

  bool hasErrors() {
    return e != null;
  }
}
