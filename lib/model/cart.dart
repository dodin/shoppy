import 'package:shoppy_template/ui/cart/cart_item.dart';

import 'dto/item.dart';

class Cart {
  List<CartItem> _items = [];
  List<CartItem> get items => _items;

  double get totalPrice => _items.fold(0, (value, element) =>
      value + element.count * double.parse(element.item.price));

  Cart({List<Item> items = const []}) {
    _prepareCart(items);
  }

  void _prepareCart(List<Item> items) {
    List<CartItem> cart = [];
    Set<String> processedSkus = {};

    for (var item in items) {
      if (processedSkus.contains(item.sku)) {
        continue;
      }

      final count = _countItems(items, item.sku);
      cart.add(CartItem(item, count));
      processedSkus.add(item.sku);
    }

    _items = cart;
  }

  int _countItems(List<Item> items, String sku) {
    return items.fold(0, (previousValue, element) => element.sku == sku ? ++previousValue : previousValue);
  }

  void delete(int position) {
    _items.removeAt(position);
  }

  void add(Item item) {
    final cartItem = _items.firstWhere((element) => element.item.sku == item.sku, orElse: () => null);
    _items.add(
        cartItem == null ? CartItem(item, 1) : CartItem(item, cartItem.count++)
    );
  }
}