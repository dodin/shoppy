
import 'dto/item.dart';
import 'dto/order.dart';
import 'dto/profile.dart';

class Repository {
  // ignore: missing_return
  Future<List<Item>> loadData() async {}

  // ignore: missing_return
  Future<List<Item>> loadCart() async {}

  // ignore: missing_return
  Future<List<Item>> addToCart(Item item) async {}

  // ignore: missing_return
  Future<List<Item>> deleteFromCart(Item item) async {}

  // ignore: missing_return
  Future<void> clearCart() async {}

  // ignore: missing_return
  Future<Set<String>> loadCategories() async {}

  // ignore: missing_return
  Future<List<Item>> loadFavorites() async {}

  // ignore: missing_return
  Future<List<Item>> toggleFavorite(Item item) async {}

  // ignore: missing_return
  bool isFavorite(Item item) {}

  // ignore: missing_return
  Future<Set<String>> loadShippingMethods() async {}

  // ignore: missing_return
  Future<Set<String>> loadPaymentMethods() async {}

  // ignore: missing_return
  Future<bool> saveOrder(Order order) async {}

  // ignore: missing_return
  Future<void> updateProfile({String address}) async {}

  // ignore: missing_return
  Future<Profile> loadProfile() async {}

  // ignore: missing_return
  Future<List<Order>> loadOrderHistory() async {}
}
