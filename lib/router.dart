import 'package:flutter/material.dart';
import 'package:shoppy_template/ui/cart/cart_page.dart';
import 'package:shoppy_template/ui/favorites/favorites_page.dart';
import 'package:shoppy_template/ui/user/account_page.dart';

import 'ui/checkout/checkout_page.dart';
import 'ui/home/main_screen.dart';
import 'ui/item/item_page.dart';

class Routes {
  static const String loginPage = '/login';
  static const String mainPage = '/main';
  static const String itemPage = '/item';
  static const String cartPage = '/cart';
  static const String checkoutPage = '/checkout';
  static const String favoritesPage = '/favorites';
  static const String accountPage = '/account';
}

class Router {
  static Route<dynamic> onGenerateRoute(RouteSettings settings) {
    final arg = settings.arguments;
    switch (settings.name) {
      case Routes.mainPage:
        return MaterialPageRoute<dynamic>(
            builder: (_) => MainScreen(),
            settings: settings
        );
      case Routes.itemPage:
        return MaterialPageRoute<dynamic>(
            builder: (_) => ItemPage(arg),
            settings: settings
        );
      case Routes.cartPage:
        return MaterialPageRoute<dynamic>(
          builder: (_) => CartPage(),
          settings: settings
        );
      case Routes.checkoutPage:
        return MaterialPageRoute<dynamic>(
            builder: (_) => CheckoutPage(),
            settings: settings
        );
      case Routes.favoritesPage:
        return MaterialPageRoute<dynamic>(
            builder: (_) => FavoritesPage(),
            settings: settings
        );
      case Routes.accountPage:
        return MaterialPageRoute<dynamic>(
            builder: (_) => AccountPage(),
            settings: settings
        );
      default:
        return null;
    }
  }
}
