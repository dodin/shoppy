import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';

ThemeData themeData() => defaultTargetPlatform == TargetPlatform.iOS
    ? kIOSTheme
    : kDefaultTheme;

final ThemeData kIOSTheme = ThemeData(
  visualDensity: VisualDensity.adaptivePlatformDensity,
  primarySwatch: Colors.cyan,
  primaryColor: Colors.lightBlue[800],
  primaryColorBrightness: Brightness.light,
);

final ThemeData kDefaultTheme = ThemeData(
  visualDensity: VisualDensity.adaptivePlatformDensity,
  primaryColor: Colors.lightBlue[800],
  accentColor: Colors.cyan[600],
);
