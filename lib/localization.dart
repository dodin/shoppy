import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class AppLocalizations {
  AppLocalizations(this.locale);

  final Locale locale;

  static AppLocalizations of(BuildContext context) {
    return Localizations.of<AppLocalizations>(context, AppLocalizations);
  }

  static Map<String, Map<String, String>> _localizedValues = {
    'en': {
      'appName': 'Shoppie',
      'navHome': 'Home',
      'navCategories': 'Categories',
      'navCart': 'Cart',
      'navAccount': 'Account',
      'cartTitle': 'Cart',
      'cartEmpty': 'Empty cart',
      'cartTotal': 'Total price',
      'cartCheckout': 'Checkout',
      'fav': 'Favorites',
      'favEmpty': 'You have no favorites yet',
      'checkoutTitle': 'Checkout',
      'checkoutAddress': 'Delivery address',
      'checkoutPayment': 'Payment method',
      'checkoutShipping': 'Shipping',
      'checkoutOrder': 'Order',
      'accountTitle': 'Account',
      'notLoggedIn': 'Currently you are not logged in.',
      'accountEmail': 'Enter your email',
      'accountPassword': 'Enter your password',
      'accountLogin': 'Login',
      'accountRegister': 'Register',
      'accountLoggedIn': 'You logged in as: ',
      'accountAddress': 'Enter your address',
      'accountOrders' : 'Your orders:',
      'accountNoOrders' : 'You have no orders yet.',
      'errorEmailEmpty' : 'Email should not be empty.',
      'errorWrongEmail' : 'Wrong email address.',
      'errorPasswordEmpty' : 'Password should not be empty',
      'errorTryAgain': 'Error! Please try again.',
    },
    'ru': {
      'appName': 'Shoppie',
      'navHome': 'Главная',
      'navCategories': 'Категории',
      'navCart': 'Корзина',
      'navAccount': 'Аккаунт',
      'cartTitle': 'Корзина',
      'cartEmpty': 'Корзина пуста',
      'cartTotal': 'Всего',
      'cartCheckout': 'Оформить заказ',
      'fav': 'Избранные',
      'favEmpty': 'У вас пока нет любимых товаров',
      'checkoutTitle': 'Заказ',
      'checkoutAddress': 'Адрес доставки',
      'checkoutPayment': 'Способ оплаты',
      'checkoutShipping': 'Доставка',
      'checkoutOrder': 'Заказать',
      'accountTitle': 'Аккаунт',
      'notLoggedIn': 'Вы не вошли в приложение.',
      'accountEmail': 'Введите ваш email',
      'accountPassword': 'Введите ваш пароль',
      'accountLogin': 'Войти',
      'accountRegister': 'Зарегистрироваться',
      'accountLoggedIn': 'Вы вошли как:',
      'accountAddress': 'Введите ваш адрес',
      'accountOrders' : 'История заказов:',
      'accountNoOrders' : 'У вас пока нет заказов.',
      'errorEmailEmpty' : 'Email не может быть пуст.',
      'errorWrongEmail' : 'Неправильный email.',
      'errorPasswordEmpty' : 'Пароль не может быть пуст.',
      'errorTryAgain': 'Произошла ошибка! Попробуйте ещё раз.',
    },
  };

  String get appName => _localizedValues[locale.languageCode]['appName'];
  String get navHome => _localizedValues[locale.languageCode]['navHome'];
  String get navCategories => _localizedValues[locale.languageCode]['navCategories'];
  String get navCart => _localizedValues[locale.languageCode]['navCart'];
  String get navAccount => _localizedValues[locale.languageCode]['navAccount'];
  String get cartTitle => _localizedValues[locale.languageCode]['cartTitle'];
  String get cartEmpty => _localizedValues[locale.languageCode]['cartEmpty'];
  String get cartTotal => _localizedValues[locale.languageCode]['cartTotal'];
  String get cartCheckout => _localizedValues[locale.languageCode]['cartCheckout'];
  String get fav => _localizedValues[locale.languageCode]['fav'];
  String get favEmpty => _localizedValues[locale.languageCode]['favEmpty'];
  String get checkoutTitle => _localizedValues[locale.languageCode]['checkoutTitle'];
  String get checkoutAddress => _localizedValues[locale.languageCode]['checkoutAddress'];
  String get checkoutPayment => _localizedValues[locale.languageCode]['checkoutPayment'];
  String get checkoutShipping => _localizedValues[locale.languageCode]['checkoutShipping'];
  String get checkoutOrder => _localizedValues[locale.languageCode]['checkoutOrder'];
  String get accountTitle => _localizedValues[locale.languageCode]['accountTitle'];
  String get notLoggedIn => _localizedValues[locale.languageCode]['notLoggedIn'];
  String get accountEmail => _localizedValues[locale.languageCode]['accountEmail'];
  String get accountPassword => _localizedValues[locale.languageCode]['accountPassword'];
  String get accountLogin => _localizedValues[locale.languageCode]['accountLogin'];
  String get accountRegister => _localizedValues[locale.languageCode]['accountRegister'];
  String get accountLoggedIn => _localizedValues[locale.languageCode]['accountLoggedIn'];
  String get accountAddress => _localizedValues[locale.languageCode]['accountAddress'];
  String get accountOrders => _localizedValues[locale.languageCode]['accountOrders'];
  String get accountNoOrders => _localizedValues[locale.languageCode]['accountNoOrders'];
  String get errorEmailEmpty => _localizedValues[locale.languageCode]['errorEmailEmpty'];
  String get errorWrongEmail => _localizedValues[locale.languageCode]['errorWrongEmail'];
  String get errorPasswordEmpty => _localizedValues[locale.languageCode]['errorPasswordEmpty'];
  String get errorTryAgain => _localizedValues[locale.languageCode]['errorTryAgain'];
}

class AppLocalizationsDelegate extends LocalizationsDelegate<AppLocalizations> {
  const AppLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) => ['en', 'ru'].contains(locale.languageCode);

  @override
  Future<AppLocalizations> load(Locale locale) {
    return SynchronousFuture<AppLocalizations>(AppLocalizations(locale));
  }

  @override
  bool shouldReload(AppLocalizationsDelegate old) => false;
}
